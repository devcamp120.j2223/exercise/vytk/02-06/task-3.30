// Lệnh này tương tự import express from 'express' // Dùng để import thư viện express vào project
const { response } = require('express');
const express = require('express');

// Khởi tạo app express
const app=express();

//Cấu hình app đọc được body request JSON
app.use(express.json());
//Cấu hình app đọc được tieng Viet uft8
app.use(express.urlencoded({
    extended:true
}));


// Khai báo cổng project
const port=8000;

// Khai báo API dạng get '/'

//Call back function: Là một tham số của hàm khác và nó sẽ được thực thi sau khi hàm đó được call
app.get('/',(request,response) => {
    let today=new Date();

    response.status(200).json({
        message:`Xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`
    });
})

app.get('/request-params/:param1',(request,response)=>{
    let param1=request.params.param1;
    response.status(200).json({
        param1:param1
    });
});

app.get('/request-query',(request,response)=>{
    let query=request.query;
    response.status(200).json({
        queryRequest:query
    });
});

app.post('/request-body',(request,response)=>{
    let body=request.body;
    response.status(200).json({
        bodyRequest:body
    });
});

//Chạy app express
app.listen(port,()=>{
    console.log(`App listening on port: ${port}`);
})
